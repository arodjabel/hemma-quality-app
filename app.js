const express = require('express');
const helmet = require('helmet');
const init = require('./admin/initFirebase');
const bodyParser = require('body-parser');
const routeFns = require('./admin/routeFns');

const port = process.env.PORT || 3000;
const app = express();

init.init();
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use('/shared_libraries/', express.static(__dirname + '/dist/shared_libraries/'));
app.use('/images/', express.static(__dirname + '/dist/shared_libraries/'));
app.use('/bundle.js', express.static(__dirname + '/dist/bundle.js'));

app.post('/api/deleteUser', routeFns.deleteUserFn);
app.post('/api/createUser', routeFns.createUserFn);
app.get('/api/forms/pdsa/:team/tasks', routeFns.getTasksFn);
app.get('/api/historical/list', routeFns.getHistoricalDates);

app.use('/*', (req, res) => {
    res.sendFile(__dirname + '/dist/index.html')
});

app.listen(port);