const admin = require("firebase-admin");
const historicBase = 'historic_list';

function getCopyOfWholeDb() {
    return admin.database().ref('/');
}

function cleanSnapshot(_snapshot) {
    const snapshot = _snapshot.val();
    delete snapshot['historic_list'];
    return snapshot;
}

function todayTimeStamp() {
    const date = new Date();
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    return date.getFullYear() + '-' + month + '-' + date.getDate();
}

function postNewHistoricObj(_snapshot) {
    return new Promise(function (resolve, reject) {
        const historicKey = todayTimeStamp();
        const dbUrl = historicBase + '/' + historicKey;
        const dbRef = admin
            .database()
            .ref()
            .child(dbUrl);
        const newHistoricNode = dbRef
            .update(_snapshot)
            .then(resolve)
            .catch(reject);
    });
}

function getHistoricalDates() {
    return new Promise(function (resolve, reject) {
        function snapSuccess(_snapshot) {
            const snapshot = _snapshot.val();
            const dates = Object.keys(snapshot);
            resolve(dates);
        }

        const dbRef = admin
            .database()
            .ref(historicBase)
            .once("value", snapSuccess, reject);
    });
}

module.exports = {
    getCopyOfWholeDb,
    cleanSnapshot,
    postNewHistoricObj,
    getHistoricalDates
};