require('./howTo.scss');

import template from './howTo.html';

export default angular.module('howTo', [])
    .directive('howTo', [function () {
        return {
            template: template,
            bindToController: true,
            controllerAs: 'howToCtrl',
            controller: [function () {

            }]
        }
    }])
    .directive('howToLink', ['$location', function ($location) {
        return {
            template: '<a href="#" ng-click="goToPage()" class="header-link">How To</a>',
            replace: true,
            link: function ($scope) {
                $scope.goToPage = () => {
                    $location.path('/how-to');
                }
            }
        }
    }])
    .name;