const template = require('./sidebar.html');
require('./sidebar.scss');

export default angular.module('sidebar', [])
    .directive('sidebar', ['$location', 'sidebarPlaces',
        function ($location, sidebarPlaces) {
        return {
            template,
            bindToController: true,
            controllerAs: 'sideCtrl',
            controller: ['$scope', function ($scope) {
                const sideCtrl = this;
                const elem = angular.element(document.querySelector('.sidebar-menu-items'));

                sideCtrl.sideItems = sidebarPlaces;
                sideCtrl.showSidebarClass = false;
                $scope.activePath = '';

                sideCtrl.changePages = (path) => {
                    $location.path(path);
                    sideCtrl.showSidebarClass = false;
                    elem.css('display', null);
                };

                sideCtrl.showSidebar = () => {
                    sideCtrl.showSidebarClass = !sideCtrl.showSidebarClass;
                    const displayPropValue = (sideCtrl.showSidebarClass) ? 'block' : null;
                    elem.css('display', displayPropValue);
                };

                const routeChange = $scope.$on('$routeChangeSuccess', () => {
                    $scope.activePath = $location.path();
                });

                $scope.$on('$destroy', routeChange);
            }]
        }
    }])
    .constant('sidebarPlaces', [
        {
            label: 'Home',
            path: '/home'
        },
        {
            label: 'Quality',
            path: '/quality',
            // subMenu: [
            //     {
            //         label: 'PDSA',
            //         path: '/quality'
            //     }
            // ]
        }
    ])
    .name;