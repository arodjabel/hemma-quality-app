require('./breadcrumbs.css');
const breadcrumbTemplate = require('./breadcrumbs.html');
export default angular.module('directives.breadcrumbs', [])
    .directive('breadcrumbs', ['$rootScope', '$location', function ($rootScope, $location) {
        return {
            template: breadcrumbTemplate,
            bindToController: true,
            controllerAs: 'crumbCtrl',
            controller: [function () {
                const crumbCtrl = this;
                const locationArr = $location.path().split('?')[0];

                crumbCtrl.locationArr = locationArr.split('/');
                crumbCtrl.locationArr.shift();
                if (crumbCtrl.locationArr[0] !== 'home') {
                    crumbCtrl.locationArr.unshift('home');
                }

                crumbCtrl.changeLocations = (index) => {
                    let newPath = '';
                    crumbCtrl.locationArr.forEach((loc, i) => {
                        if (i <= index) {
                            newPath += '/' + loc;
                        }
                    });

                    if (!newPath) {
                        newPath = '/';
                    }

                    $location.path(newPath);
                };
            }]
        }
    }])
    .name;