var admin = require("firebase-admin");
var serviceAccount = require("./hemmahealthquality-847e47e58723.json");

function init() {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://hemmahealthquality.firebaseio.com"
    });

    return admin;
}

module.exports = {init};