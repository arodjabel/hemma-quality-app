require('./admin-team-meeting.scss');
const template = require('./admin-team-meeting.html');

export default angular.module('admin.teamMeeting', [])
.directive('adminTeamMeeting', ['patchData', function (patchData) {
    return {
        bindToController: true,
        template: template,
        controllerAs: 'teamMtgCtrl',
        controller: ['$scope', function($scope) {
            const teamMtgCtrl = this;
            teamMtgCtrl.meetingEditUpdateType = 'edit';

            function meetingEditUpdate(type, modelName) {
                if (type === 'edit' || type === 'update') {
                    teamMtgCtrl.meetingEditUpdateType = type;
                }

                if (modelName && teamMtgCtrl.methods.model[modelName]) {
                    // post this model
                    teamMtgCtrl.methods.updateMeetingOccurence(modelName);
                }
            }

            teamMtgCtrl.meetingEditUpdate = meetingEditUpdate;
        }],
        scope: {
            methods: '=adminTeamMeeting',
            team: '=teamName'
        }
    }
}])
.name