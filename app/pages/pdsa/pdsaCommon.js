export default angular.module('pdsa.common', [])
    .factory('pdsa.convertDbObjToArray', [() => {
        return function (arr) {
            const response = [];
            let row;
            Object.keys(arr).forEach((key) => {
                row = arr[key];
                row.imageClass = '';
                row.id = key;
                row.insertDate = Date.parse(row.insertDate);
                row.lastUpdatedDate = Date.parse(row.lastUpdatedDate);
                row.deactive = row.deactive || false;
                response.push(row);
            });
            return response;
        }
    }])
    .factory('pdsa.findNextTwelveMeetings', [
        'monthLookup',
        'pdsa.storeIt',
        'pdsaAgenda.convertMeetingObjToDateObj',
        function (monthLookup, storeItService, convertMeetingObjToDateObj) {
            return function (meetingObj) {
                let i;
                const date = new Date();
                const response = [];

                if(!meetingObj.weekOfMonth || !meetingObj.dayOfWeek){
                    return response;
                }

                function getLabel(aDate){
                    let response = 'QI Meeting - ';
                    response += `${monthLookup[aDate.getMonth()]} ${aDate.getFullYear()}`;
                    return response;
                }

                for (i = 0; i < 12; i += 1) {
                    const dateToSend = date.setMonth(date.getMonth() + 1);
                    const dateObj = convertMeetingObjToDateObj(meetingObj, date);
                    const month = `0${dateObj.getMonth() + 1}`.slice(-2);
                    const day = `0${dateObj.getDate()}`.slice(-2);
                    const option = {
                        label: getLabel(dateObj),
                        date: `${dateObj.getFullYear()}-${month}-${day}`
                    };
                    response.push(option);
                }
                return response;
            }
        }]
    )
    .factory('pdsa.getPdsaData', [
        'firebase',
        '$q',
        '$http',
        '$location',
        '$rootScope',
        function (firebase, $q, $http, $location, $rootScope) {
            return function (team) {
                const deferred = $q.defer();

                function successCb(result) {
                    deferred.resolve(result.data);
                }

                function failCb(error) {
                    deferred.reject(error);
                }

                function doGet(idToken) {
                    const url = `https://hemmahealthquality.firebaseio.com/forms/pdsa/${team}` +
                        '.json?print=pretty&auth=' + idToken;
                    $http.get(url).then(successCb, failCb);
                }

                if(!team){
                    failCb('missing team');
                } else {
                    firebase.auth().currentUser.getToken(true).then(function (idToken) {
                        doGet(idToken);
                    }).catch(function (error) {
                        $location.path('/login');
                    });
                }

                return deferred.promise;
            }
        }]
    )
    .name