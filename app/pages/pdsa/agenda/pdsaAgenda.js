require('./pdsaAgenda.scss');

const common = require('../../../../admin/commonFn.js');
import agendaTemplate from './pdsaAgenda.html';

export default angular.module('pdsa.agenda', [])
    .directive('pdsaAgenda', [
        '$sanitize',
        '$rootScope',
        '$window',
        '$routeParams',
        'getData',
        'patchData',
        'postData',
        'deleteData',
        'pdsa.getPdsaData',
        'pdsaAgenda.handlePdsaDataConversion',
        'pdsaAgenda.convertDataToList',
        'pdsaAgenda.getDateInfo',
        'pdsaAgenda.convertDateObjToDateStr',
        'pdsa.getAdminData',
        function ($sanitize, $rootScope, $window, $routeParams, getData,
                  patchData, postData, deleteData,
                  getPdsaData, handlePdsaDataConversion, convertDataToList,
                  getDateInfo, convertDateObjToDateStr,
                  getAdminData) {
            return {
                template: agendaTemplate,
                bindToController: true,
                controllerAs: 'agendaCtrl',
                controller: ['$scope', function ($scope) {
                    const agendaCtrl = this;
                    const topicEndpoint = 'https://hemmahealthquality.firebaseio.com/qualityMeetingData/qiMeetingAdditonalTopics';

                    agendaCtrl.filterModel = {};
                    agendaCtrl.newTopicModel = {};
                    agendaCtrl.toggleAddTopicButton = false;
                    agendaCtrl.borderClass = 'agenda-topic-form-regular-animation';

                    agendaCtrl.print = () => {
                        $window.print();
                    };

                    agendaCtrl.addNewAgendaTopic = () => {
                        const agendaTopic = validator();
                        postData(topicEndpoint, agendaTopic).then(init, failCb);
                    };

                    agendaCtrl.doTheUpdateDelete = () => {
                        const agendaTopic = validator();
                        const dataToPatch = {};
                        dataToPatch[agendaCtrl.newTopicModel.key] = agendaTopic;
                        // actually do the update or delete
                        if (agendaCtrl.toggleAddTopicButton === 'Update') {
                            patchData(topicEndpoint, dataToPatch).then(init, failCb);
                        } else {
                            deleteData(topicEndpoint, agendaCtrl.newTopicModel.key).then(init, failCb);
                        }
                    };

                    agendaCtrl.updateDeleteAgendaTopic = (type, topic) => {
                        // toggle add topic button
                        agendaCtrl.toggleAddTopicButton = type;
                        agendaCtrl.borderClass = (type === 'Update') ? 'agenda-topic-form-update-animation' : 'agenda-topic-form-delete-animation';
                        agendaCtrl.topicFormTitleText = (type === 'Update') ? 'Make A Change To This Topic' : 'Are You Sure? Delete Cannot Be Undone';

                        // auto fill form
                        agendaCtrl.newTopicModel.key = topic.key;
                        agendaCtrl.newTopicModel.shortDesc = topic.shortDesc;
                        agendaCtrl.newTopicModel.longDesc = topic.longDesc;
                    };

                    agendaCtrl.cancelUpdate = () => {
                        agendaCtrl.toggleAddTopicButton = false;
                        agendaCtrl.borderClass = 'agenda-topic-form-regular-animation';
                        agendaCtrl.topicFormTitleText = 'Add New Topic';
                        agendaCtrl.newTopicModel.key = '';
                        agendaCtrl.newTopicModel.shortDesc = '';
                        agendaCtrl.newTopicModel.longDesc = '';
                    };

                    agendaCtrl.updateDateOptions = () => {
                        getAdminData().then(useAdminData, failCb);
                    };

                    function addTopicsToView(data) {
                        agendaCtrl.agendaTopicList = convertDataToList(data.data);
                    }

                    function addDateToView(data) {
                        agendaCtrl.nextMeetingStr = '';
                        if (!data.dayOfWeek || !data.weekOfMonth) {
                            agendaCtrl.nextMeetingStr = 'No Date Information';
                            return;
                        }
                        const dateStr = convertDateObjToDateStr(data);
                        if (dateStr) {
                            agendaCtrl.nextMeetingStr = dateStr;
                        }
                    }

                    function figureOutDisplayDate(res) {
                        const selectedTeam = (agendaCtrl.filterModel.team) ? agendaCtrl.filterModel.team.name : 'default';
                        const meetingObj = res.data.dates.qualityMeeting[selectedTeam];

                        addDateToView(meetingObj);
                    }

                    function addTeamsToView(res) {
                        agendaCtrl.teamNameSelectOptions = res.data.teamNames;
                    }

                    function useAdminData(res) {
                        addTeamsToView(res);
                        figureOutDisplayDate(res);
                    }

                    function failCb(err) {
                        console.log(err);
                    }

                    function init() {
                        if (!$routeParams.team) {
                            return;
                        }
                        agendaCtrl.toggleAddTopicButton = '';
                        agendaCtrl.newTopicModel.key = '';
                        agendaCtrl.newTopicModel.shortDesc = '';
                        agendaCtrl.newTopicModel.longDesc = '';
                        agendaCtrl.borderClass = 'agenda-topic-form-regular-animation';
                        agendaCtrl.topicFormTitleText = 'Add New Topic';
                        getPdsaData($routeParams.team).then(pdsaDataCb, failCb);
                        getData(topicEndpoint).then(addTopicsToView, failCb);
                        getAdminData().then(useAdminData, failCb);
                    }

                    function validator() {
                        return {
                            shortDesc: $sanitize(agendaCtrl.newTopicModel.shortDesc),
                            longDesc: $sanitize(agendaCtrl.newTopicModel.longDesc)
                        };
                    }

                    function pdsaDataCb(data) {
                        agendaCtrl.list = handlePdsaDataConversion(data);
                    }

                    function getSomeHistData(event, payload) {
                        if (payload === '--no selection--') {
                            init();
                            return;
                        }

                        function successCb(data) {
                            pdsaDataCb(data.data.forms.pdsa[$routeParams.team]);
                            addTopicsToView({data: data.data.qualityMeetingData.qiMeetingAdditonalTopics});
                            agendaCtrl.nextMeetingStr = `${payload} (Historic Agenda)`;
                        }

                        const endpoint = `https://hemmahealthquality.firebaseio.com/historic_list/${payload}`;
                        getData(endpoint).then(successCb, failCb);
                    }

                    $scope.$on('historicDateChage', getSomeHistData);

                    init();
                }]
            }
        }
    ])
    .factory('pdsaAgenda.convertMeetingObjToDateObj',
        function () {
            return common.convertMeetingObjToDateObj
        }
    )
    .factory('pdsaAgenda.convertDateObjToDateStr',
        function () {
            return common.convertMeetingObjToString;
        }
    )
    .factory('pdsaAgenda.getWeekdaysInMonth',
        function () {
            return common.getWeekdaysInMonth;
        }
    )
    .factory('pdsaAgenda.getDateInfo', ['$q', 'getData', function ($q, getData) {
        return function () {
            const deferred = $q.defer();
            const endpoint = 'https://hemmahealthquality.firebaseio.com/admin/dates/qualityMeeting';

            function successCb(data) {
                deferred.resolve(data.data);
            }

            function failureCb(data) {
                deferred.reject(data);
            }

            getData(endpoint).then(successCb, failureCb);
            return deferred.promise;
        }
    }])
    .factory('pdsaAgenda.convertDataToList', [function () {
        return function (dataObj) {
            const dataKeys = Object.keys(dataObj);
            const response = [];
            let tempObj;
            dataKeys.forEach((key) => {
                tempObj = angular.copy(dataObj[key]);
                tempObj.key = key;
                response.push(tempObj);
            });

            return response;
        }
    }])
    .factory('pdsaAgenda.handlePdsaDataConversion', [
        'pdsa.convertDbObjToArray',
        function (convertDbObjToArray) {
            return function (payload) {
                function filterFn(row) {
                    // TODO only want rows with tasks due in this period
                    return !row.deactive;
                }

                const response = convertDbObjToArray(payload).filter(filterFn);

                return response;
            }
        }
    ])
    .name;