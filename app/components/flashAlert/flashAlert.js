require('./flashAlert.css');

export default angular.module('directives.flashAlert', [])
    .directive('flashAlert', ['$timeout', 'alertLookup', function ($timeout, alertLookup) {
        return {
            template: '<div class="flash-alert-container"><span>{{flashCtrl.alert}}</span></div>',
            bindToController: true,
            controllerAs: 'flashCtrl',
            controller: ['$scope', function ($scope) {
                const flashCtrl = this;

                function showAlert(newVal, oldVal) {
                    if (newVal === oldVal) {
                        return;
                    }

                    let alert;
                    if (alertLookup[newVal]) {
                        alert = alertLookup[newVal];
                    } else {
                        alert = 'something went wrong';
                    }

                    flashCtrl.alert = alert;

                    $timeout(function () {
                        flashCtrl.alert = '';
                    }, 5000);
                }

                $scope.$watch('flashCtrl.flashAlert', showAlert);
            }],
            scope: {
                flashAlert: '=flashAlert'
            }
        }
    }])
    .constant('alertLookup', {
        'auth/email-already-exists': 'Email Address Already Exists',
        'successfulPasswordReset': 'Password Reset Has Been Sent To User',
        'duplicateTeamName': 'Team Name is already in the list'
    })
    .name;
