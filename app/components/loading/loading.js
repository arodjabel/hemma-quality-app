require('./loading.css');

export default angular.module('quality.loading', [])
    .directive('loading', [function () {
        return {
            template: '<div  class="spinner-container">' +
            '<div class="spinner" ng-show="loading">' +
            '<div class="bounce1 ball"></div>' +
            '<div class="bounce2 ball"></div>' +
            '<div class="bounce3 ball"></div>' +
            '<div class="bounce4 ball"></div>' +
            '<div class="bounce5 ball"></div>' +
            '</div>' +
            '<ng-transclude></ng-transclude>' +
            '</div>',
            transclude: true,
            scope: {
                loading: '='
            }
        }
    }])
    .name;