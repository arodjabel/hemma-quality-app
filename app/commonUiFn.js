export default angular.module('quality.commonUiFn', [])
    .factory('common.iterateObj', [function () {
        return function* (obj) {
            for (let key of Object.keys(obj)) {
                yield [key, obj[key]];
            }
        }
    }])
    .name;