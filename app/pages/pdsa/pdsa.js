require('./pdsa.scss');

import listViewTemplate from './pdsaListView.html';
import homeViewTemplate from './pdsaHome.html';
import pdsaUpdateTemplate from './pdsaUpdate.html';
import pdsaFormTemplate from './pdsaForm.html';
import pdsaCreateTemplate from './pdsaCreate.html';
import pdsaCommon from './pdsaCommon';
import pdsaAgenda from './agenda/pdsaAgenda';
import historicPicker from './historicalPicker/historicalPicker';

export default angular.module('directive.pdsa', [
    pdsaAgenda,
    pdsaCommon,
    historicPicker])
    .directive('pdsaHome', [
        '$rootScope', '$routeParams', '$location', 'pdsa.getAdminData', 'pdsa.storeIt',
        function ($rootScope, $routeParams, $location, getAdminData, storeItService) {
            return {
                template: homeViewTemplate,
                bindToController: true,
                controllerAs: 'pdsaCtrl',
                controller: ['$scope', function ($scope) {
                    const pdsaCtrl = this;
                    $scope.showUpdateModal = false;
                    pdsaCtrl.team = $routeParams.team;

                    if ($routeParams.tab === 'agenda') {
                        pdsaCtrl.selectedPill = '0';
                    } else if ($routeParams.tab === 'plan') {
                        pdsaCtrl.selectedPill = '1';
                    } else if ($routeParams.tab === 'update') {
                        pdsaCtrl.selectedPill = '2';
                    } else if ($routeParams.tab === 'create') {
                        pdsaCtrl.selectedPill = '3';
                    } else {
                        $location.path('/quality');
                    }

                    function showUpdateCb(event, payload) {
                        $scope.showUpdateModal = !$scope.showUpdateModal;
                        if (payload && payload.obj) {
                            pdsaCtrl.pdsaToUpdate = payload.obj;
                        }
                    }

                    function storeIt(response) {
                        storeItService.setByKey(response.data.teamNames, 'teamNames');
                    }

                    function init() {
                        getAdminData().then(storeIt, () => {
                            console.log('cannot get team names')
                        })
                    }

                    const pdsaShowModal = $rootScope.$on('pdsaShowHideUpdateModal', showUpdateCb);

                    $scope.$on('$destroy', function () {
                        pdsaShowModal();
                    });

                    init();
                }]
            }
        }
    ])
    .directive('pdsaListView', [
        '$rootScope',
        '$location',
        '$timeout',
        '$routeParams',
        'pdsa.getPdsaData',
        'pdsa.deletePdsaData',
        'pdsa.convertDbObjToArray',
        ($rootScope, $location, $timeout, $routeParams, getPdsaData, deletePdsaData, convertDbObjToArray) => {
            return {
                template: listViewTemplate,
                bindToController: true,
                controllerAs: 'listCtrl',
                controller: ['$scope', function ($scope) {
                    const listCtrl = this;

                    if (!$routeParams.team) {
                        $location.path('/quality');
                        return;
                    }

                    function pdsaDataCb(data) {
                        $scope.mostRecentData = data;
                        listCtrl.data = convertDbObjToArray(data);
                    }

                    function init() {
                        getPdsaData($routeParams.team).then(pdsaDataCb, (err) => {
                            console.log(err)
                        });
                    }

                    listCtrl.openThisPdsa = (obj, index) => {
                        $rootScope.$broadcast('pdsaShowHideUpdateModal', {obj, index});
                    };

                    listCtrl.deleteThis = (event, item, id) => {
                        event.stopPropagation();
                        listCtrl.idToDelete = id;
                        listCtrl.deleteAlert = true;
                    };

                    listCtrl.dontDelete = () => {
                        listCtrl.idToDelete = '';
                        listCtrl.deleteAlert = false;
                    };

                    listCtrl.deleteIt = () => {
                        const team = $scope.mostRecentData[listCtrl.idToDelete].team.name;
                        deletePdsaData(team, listCtrl.idToDelete);
                        listCtrl.deleteAlert = false;
                    };

                    init();
                    $scope.$on('reloadPdsaListView', init);
                }],
                link: function ($scope) {
                    const location = $location.search();

                    function checkForQueryParam() {
                        if (location.key && $scope.mostRecentData) {
                            const pdsaKey = location.key;
                            $location.search('key', null);
                            $timeout(() => {
                                    $scope.listCtrl.openThisPdsa($scope.mostRecentData[pdsaKey], pdsaKey)
                                }, 0
                            );
                        }
                    }

                    $scope.$watch('mostRecentData', (newVal, oldVal) => {
                        if (newVal === oldVal) {
                            return
                        }
                        checkForQueryParam();
                    });
                }
            }
        }
    ])
    .directive('pdsaUpdate', ['$rootScope', function ($rootScope) {
        return {
            template: pdsaUpdateTemplate,
            scope: {
                pdsaToUpdate: '=pdsaUpdate'
            },
            bindToController: true,
            controllerAs: 'pdsaUpdateCtrl',
            controller: ['$scope', function ($scope) {
                const pdsaUpdateCtrl = this;
                pdsaUpdateCtrl.hideModal = () => {
                    $rootScope.$broadcast('pdsaShowHideUpdateModal');
                };
            }]
        }
    }])
    .directive('pdsaForm', [
        '$q',
        '$routeParams',
        'pdsa.storeIt',
        'pdsa.postPdsaData',
        'pdsaFrequencyList',
        'pdsaTypeList',
        'userRoleService',
        'pdsa.findNextTwelveMeetings',
        'pdsa.getAdminData',
        function ($q, $routeParams, storeItService, postPdsaData, pdsaFrequencyList, pdsaTypeList, userRoleService, findNextTwelveMeetings, getAdminData) {
            return {
                template: pdsaFormTemplate,
                scope: {
                    pdsaToUpdate: '=pdsaForm'
                },
                bindToController: true,
                controllerAs: 'pdsaFormCtrl',
                controller: ['$scope', function ($scope) {
                    const pdsaFormCtrl = this;
                    const planOneRowTemplate = {
                        task: '',
                        party: '',
                        date: '',
                        location: '',
                        taskComplete: false
                    };

                    pdsaFormCtrl.frequencySelectOptions = pdsaFrequencyList;
                    pdsaFormCtrl.formTypeSelectOptions = pdsaTypeList;

                    pdsaFormCtrl.model = {
                        act: '',
                        aim: '',
                        owner: '',
                        planOne: [
                            angular.copy(planOneRowTemplate)
                        ],
                        prediction: '',
                        frequency: '',
                        type: '',
                        measure: '',
                        outcome: '',
                        study: '',
                        team: {
                            name: $routeParams.team
                        }
                    };

                    function usePdsaToUpdateCb(newVal, oldVal) {
                        if (!pdsaFormCtrl.pdsaToUpdate) {
                            const currentRolePromise = userRoleService.getCurrentUserRoles();
                            const teamNamePromise = getAdminData();
                            $q.all([currentRolePromise, teamNamePromise]).then(init);
                            return;
                        }

                        pdsaFormCtrl.updateButtonText = 'Update';
                        pdsaFormCtrl.pdsaToUpdate.planOne.forEach((row) => {
                            if (row.date) {
                                row.date = new Date(row.date);
                            }
                        });

                        pdsaFormCtrl.oldTeam = pdsaFormCtrl.pdsaToUpdate.team;
                        pdsaFormCtrl.model = pdsaFormCtrl.pdsaToUpdate;
                        pdsaFormCtrl.model.team = {
                            name: $routeParams.team
                        };

                        const currentRolePromise = userRoleService.getCurrentUserRoles();
                        const teamNamePromise = getAdminData();
                        $q.all([currentRolePromise, teamNamePromise]).then(init);
                    }

                    function validateModel() {
                        let index;

                        const team = pdsaFormCtrl.model.team.name;
                        const oldTeam = (pdsaFormCtrl.oldTeam) ? pdsaFormCtrl.oldTeam.name : pdsaFormCtrl.model.team.name;
                        if (pdsaFormCtrl.model.insertDate) {
                            pdsaFormCtrl.model.insertDate = new Date(pdsaFormCtrl.model.insertDate);
                        } else {
                            pdsaFormCtrl.model.insertDate = new Date();
                        }

                        pdsaFormCtrl.model.lastUpdatedDate = new Date();

                        if (pdsaFormCtrl.pdsaToUpdate) {
                            index = pdsaFormCtrl.pdsaToUpdate.id;
                        }

                        postPdsaData(pdsaFormCtrl.model, team, index, oldTeam);
                    }

                    function setUpPartyDropDowns() {
                        const users = userRoleService.getRoles();
                        const keys = Object.keys(users);
                        pdsaFormCtrl.partyList = keys.map((key) => {
                            return {
                                label: `${users[key].first} ${users[key].last}`,
                                value: key
                            }
                        });
                    }

                    function setupTeamDropDowns(res) {
                        pdsaFormCtrl.teamNameSelectOptions = res.data.teamNames;
                        // have to find meeting based on team;
                        const teamName = (pdsaFormCtrl.model.team) ? pdsaFormCtrl.model.team.name : 'default';
                        const meetingObj = res.data.dates.qualityMeeting[teamName];
                        pdsaFormCtrl.futureMeetingDates = findNextTwelveMeetings(meetingObj);
                    }

                    function recordDeactiveTimeStamp() {
                        pdsaFormCtrl.model.deactiveTimeStamp = new Date();
                    }

                    function recordAccomplishmentTimeStamp() {
                        pdsaFormCtrl.model.accomplishmentTimeStamp = new Date();
                    }

                    function init(resArr) {
                        setUpPartyDropDowns();
                        setupTeamDropDowns(resArr[1]);
                    }

                    $scope.$watch('pdsaFormCtrl.pdsaToUpdate', usePdsaToUpdateCb);

                    pdsaFormCtrl.updateButtonText = 'Submit';

                    pdsaFormCtrl.recordDeactiveTimeStamp = recordDeactiveTimeStamp;
                    pdsaFormCtrl.recordAccomplishmentTimeStamp = recordAccomplishmentTimeStamp;

                    pdsaFormCtrl.addRow = () => {
                        pdsaFormCtrl.model.planOne.push(angular.copy(planOneRowTemplate));
                    };

                    pdsaFormCtrl.updateDatabase = () => {
                        validateModel();
                    };
                }]
            }
        }
    ])
    .directive('pdsaCreateView', [() => {
        return {
            template: pdsaCreateTemplate,
            bindToController: true,
            controllerAs: 'pdsaCreateCtrl',
            controller: [function () {
                const pdsaCreateCtrl = this;

            }]
        }
    }])
    .factory('pdsa.postPdsaData', ['firebase', '$q', '$http', '$rootScope', 'pdsa.deletePdsaData',
        (firebase, $q, $http, $rootScope, deletePdsaData) => {
            return function (postData, team, index, oldTeam) {
                function successCb(response) {
                    $rootScope.$broadcast('pdsaShowHideUpdateModal', 'hide modal');
                    $rootScope.$broadcast('reloadPdsaListView');
                }

                function failureCb(response) {
                    console.log(response);
                }

                function doPut(idToken) {
                    const url = `https://hemmahealthquality.firebaseio.com/forms/pdsa/${team}/${index}.json?auth=${idToken}`;
                    $http.put(url, postData).then(successCb, failureCb);
                }

                function doPost(idToken) {
                    const newTeamUrl = `https://hemmahealthquality.firebaseio.com/forms/pdsa/${team}.json?auth=${idToken}`;
                    const oldTeamUrl = `https://hemmahealthquality.firebaseio.com/forms/pdsa/${oldTeam}/${index}.json?auth=${idToken}`;

                    const arr = [];
                    arr.push($http.post(newTeamUrl, postData));
                    console.log(team, oldTeam);
                    if (team !== oldTeam) {
                        arr.push($http.delete(oldTeamUrl));
                    }

                    $q.all(arr).then(successCb, failureCb);
                }

                firebase.auth().currentUser.getToken(true).then(function (idToken) {
                    if (!index || team !== oldTeam) {
                        doPost(idToken);
                    } else {
                        doPut(idToken);
                    }
                }).catch(function (error) {
                    $location.path('/login');
                });
            }
        }])
    .factory('pdsa.deletePdsaData', [
        '$http',
        '$location',
        '$rootScope',
        'firebase',
        'pdsa.getPdsaData',
        function ($http, $location, $rootScope, firebase, getPdsaData) {
            return function (team, id) {
                function doPost(idToken) {
                    const url = `https://hemmahealthquality.firebaseio.com/forms/pdsa/${team}/${id}` +
                        '.json?print=pretty&auth=' + idToken;

                    function successCb(result) {
                        $rootScope.$broadcast('reloadPdsaListView');
                    }

                    function failCb(error) {
                        console.log(error);
                    }

                    $http.delete(url).then(successCb, failCb);
                }

                firebase.auth().currentUser.getToken(true).then(function (idToken) {
                    doPost(idToken);
                }).catch(function (error) {
                    $location.path('/login');
                });
            }
        }])
    .service('pdsa.storeIt', [function () {
        const data = {};

        function setByKey(_data, key) {
            data[key] = _data
        }

        function getByKey(key) {
            return data[key];
        }

        return {
            getByKey,
            setByKey
        }
    }])
    .constant('pdsaFrequencyList', [
        {
            key: 'weekly',
            value: 'Weekly'
        },
        {
            key: 'monthly',
            value: 'Monthly'
        },
        {
            key: 'quarterly',
            value: 'Quarterly'
        },
        {
            key: 'yearly',
            value: 'Yearly'
        }
    ])
    .constant('pdsaTypeList', [
        {
            key: 'dental',
            value: 'Dental',
            imageClass: 'icon-i-dental'
        },
        {
            key: 'medical',
            value: 'Medical',
            imageClass: 'icon-i-health-services'
        },
        {
            key: 'admin',
            value: 'Administrative',
            imageClass: 'icon-i-administration'
        }
    ])
    .name;