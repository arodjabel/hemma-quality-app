const localArcFns = require('./node/archiver.js');

const filesToCompress = ['app.js', 'package.json'];
const dirsToCompress = ['dist', 'admin'];

function runTheArchive() {
    localArcFns.methods.runTheArchive(filesToCompress, dirsToCompress);
}

runTheArchive();