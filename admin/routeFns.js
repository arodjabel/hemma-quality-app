const histFns = require('./historicData/historicMethods');
const getDataModule = require('./userManagement');
const getPdsaPlanRowTasks = require('./getPdsaTasks');

function deleteUserFn(req, res) {
    function success() {
        res.sendStatus(200);
    }

    function failure() {
        res.sendStatus(500);
    }

    if (req.body.token) {
        getDataModule.deleteUser(req.body.email).then(success, failure);
    }
}

function createUserFn(req, res) {
    const email = req.body.email;
    const password = req.body.password;
    const token = req.body.token;

    function success() {
        res.status(200).end();
    }

    function failure(error) {
        if (error === 'auth/email-already-exists') {
            res.status(200).send(error);
        } else {
            res.status(500).send(error);
        }
    }

    if (email && password && token) {
        getDataModule.createUser(email, password).then(success, failure);
    } else {
        res.status(500).end();
    }
}

function getTasksFn(req, res) {
    function successCb(data) {
        res.status(200).send(data);
    }

    function failureCb(err) {
        res.status(500).send(err);
    }

    const team = req.params.team;

    getPdsaPlanRowTasks.getTasksByChild(team).then(successCb, failureCb);
}

function getHistoricalDates(req, res) {
    function successCb(data) {
        res.status(200).send(data);
    }

    function failureCb(err) {
        res.status(500).send(err);
    }

    histFns.getHistoricalDates().then(successCb, failureCb);
}

module.exports = {
    deleteUserFn,
    createUserFn,
    getTasksFn,
    getHistoricalDates
};