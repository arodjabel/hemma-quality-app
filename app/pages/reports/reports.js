require('./reports.scss');
const reportsTemplate = require('./reports.html');
import pdsas from '../pdsa/pdsa';

export default angular.module('directives.reports', [pdsas])
    .directive('reportHome', ['$location', 'userRoleService', ($location, userRoleService) => {
        return {
            template: reportsTemplate,
            bindToController: true,
            controllerAs: 'reportCtrl',
            controller: [function () {
                const reportCtrl = this;
                const currentUserObserve = userRoleService.currentUserObservable();

                reportCtrl.goTo = (page) => {
                    $location.path(page);
                };

                function init(user) {
                    reportCtrl.teams = [];
                    if(!user.teams){
                        return;
                    }

                    reportCtrl.teams = Object.keys(user.teams);
                }

                function failure(){
                    console.log('could not get user data');
                }

                currentUserObserve.subscribe(value => {
                    console.log(value);
                    init(value);
                });
            }]
        }
    }])
    .name;