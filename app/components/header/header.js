require('./header.scss');
const headerTemplate = require('./header.html');
export default angular.module('directives.header', [])
    .directive('header', [function () {
        return {
            template: headerTemplate,
            bindToController: true,
            controllerAs: 'headCtrl',
            controller: [function () {
                const headCtrl = this;
            }]
        }
    }])
    .name;