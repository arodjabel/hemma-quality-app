require('./admin.scss');

const adminBtnTemplate = require('./admin-button.html');
const adminPageTemplate = require('./admin-home.html');
const adminRoleCheckTemplate = require('./admin-roleChecker.html');

export default angular.module('admin', [])
    .directive('admin', [
        '$rootScope',
        '$location',
        'firebase',
        'userRoleService',
        function ($rootScope, $location, firebase, userRoleService) {
            return {
                template: adminBtnTemplate,
                bindToController: true,
                controllerAs: 'adminBtnCtrl',
                replace: true,
                controller: [function () {
                    const adminBtnCtrl = this;
                    const user = firebase.auth().currentUser;
                    adminBtnCtrl.showAdmin = false;

                    function success(userObj) {
                        if (userObj && userObj.roleValue >= 400) {
                            adminBtnCtrl.showAdmin = true;

                            adminBtnCtrl.openAdmin = () => {
                                if (adminBtnCtrl.showAdmin) {
                                    $location.path('/admin');
                                }
                            };
                        } else {
                            $location.path('/home');
                        }
                    }

                    function fail() {
                        adminBtnCtrl.showAdmin = false;
                        $location.path('/home');
                    }

                    if (user) {
                        userRoleService.getCurrentUserRoles().then(success, fail);
                    } else {
                        // No user is signed in.
                        console.log('signed out');
                        $location.path('/login');
                    }
                }]
            }
        }
    ])
    .directive('adminHome', [
        '$rootScope',
        '$sanitize',
        '$q',
        'getAdminData',
        'patchData',
        'postData',
        'deleteData',
        'adminResetPassword',
        'adminCreateUser',
        'adminDeleteUser',
        function ($rootScope, $sanitize, $q,
                  getAdminData, patchData, postData,
                  deleteData, adminResetPassword, adminCreateUser,
                  adminDeleteUser) {
            return {
                template: adminPageTemplate,
                bindToController: true,
                controllerAs: 'adminCtrl',
                controller: [function () {
                    const adminCtrl = this;

                    adminCtrl.defaultTeam = {
                        name: 'default',
                        label: 'All Staff / All Teams'
                    };

                    adminCtrl.model = {
                        default: {
                            useThisMeeting: undefined,
                            weekOfMonth: undefined,
                            dayOfWeek: undefined
                        },
                        newUser: {}
                    };

                    adminCtrl.meetingEditUpdateType = 'edit';

                    function init() {
                        adminCtrl.loadingIndMeetingOccurrence = true;
                        adminCtrl.loadingIndQiMeeting = true;
                        adminCtrl.createUserLoading = true;
                        adminCtrl.model.newTeam = '';
                        getAdminData();
                    }

                    function setUpUserData(obj) {
                        if (!obj) {
                            adminCtrl.userList = [];
                            return;
                        }
                        const keys = Object.keys(obj);
                        adminCtrl.userList = [];
                        keys.forEach(function (key) {
                            obj[key].key = key;
                            adminCtrl.userList.push(obj[key]);
                        })
                    }

                    function setupModelProps(model, modelName) {
                        if (model.useThisMeeting) {
                            adminCtrl.model[modelName].useThisMeeting = true;
                        }

                        if (model.dayOfWeek) {
                            adminCtrl.model[modelName].dayOfWeek = model.dayOfWeek;
                        }

                        if (model.weekOfMonth) {
                            adminCtrl.model[modelName].weekOfMonth = model.weekOfMonth;
                        }

                        if (model.doNotSend) {
                            adminCtrl.model[modelName].meetingReminder = null;
                        } else {
                            adminCtrl.model[modelName].meetingReminder = parseInt(model.meetingReminder, 10) || null;
                        }

                        adminCtrl.model[modelName].doNotSend = model.doNotSend;
                    }

                    function setRoleList(data){
                        const roles = data;
                        const roleArr = Object.keys(roles).map((role) => {
                            return roles[role];
                        });
                        roleArr.sort((a, b) => {
                            return a.roleValue - b.roleValue;
                        });
                        adminCtrl.roleArr = roleArr;
                    }

                    function updateView(e, payload) {
                        adminCtrl.loadingIndMeetingOccurrence = false;
                        adminCtrl.loadingIndQiMeeting = false;
                        adminCtrl.createUserLoading = false;

                        const defaultModel = payload[0].data.dates.qualityMeeting.default;

                        setupModelProps(defaultModel, 'default');
                        setUpUserData(payload[1].data);
                        setTeamList(payload[0].data.teamNames);
                        setRoleList(payload[2].data);
                    }

                    function setTeamList(data) {
                        adminCtrl.teamNames = data;
                    }

                    function successCb(success) {
                        adminCtrl.updateAlert = false;
                        adminCtrl.deleteAlert = false;
                        adminCtrl.model.newUser.email = '';
                        adminCtrl.model.newUser.firstName = '';
                        adminCtrl.model.newUser.lastName = '';
                        init();
                    }

                    function failureCb(err) {
                        if (err === 'auth/email-already-exists') {
                            adminCtrl.alert = 'auth/email-already-exists';
                        }
                        successCb();
                    }

                    function sendPasswordReset(data) {
                        const url = `https://hemmahealthquality.firebaseio.com/users/`;
                        delete data.password;
                        const adminResetPassPromise = adminResetPassword(data);
                        const postPromise = postData(url, data);
                        $q.all([adminResetPassPromise, postPromise]).then(successCb, failureCb)
                    }

                    function addEmail() {
                        adminCtrl.createUserLoading = true;
                        const data = {};
                        data.email = $sanitize(adminCtrl.model.newUser.email);
                        data.password = Math.random().toString(36).slice(-8);
                        data.first = $sanitize(adminCtrl.model.newUser.firstName);
                        data.last = $sanitize(adminCtrl.model.newUser.lastName);
                        data.roleValue = 100;

                        adminCreateUser(data).then(sendPasswordReset, failureCb);
                    }

                    function updateMeetingOccurence(modelName) {
                        adminCtrl.loadingIndMeetingOccurrence = true;
                        validateModelAndPatch(adminCtrl.model[modelName], modelName);
                    }

                    function validateModelAndPatch(obj, modelName) {
                        const endpoint = `https://hemmahealthquality.firebaseio.com/admin/dates/qualityMeeting/${modelName}`;
                        const data = {};

                        data.dayOfWeek = $sanitize(obj.dayOfWeek);
                        data.weekOfMonth = $sanitize(obj.weekOfMonth);
                        data.doNotSend = obj.doNotSend;
                        data.useThisMeeting = obj.useThisMeeting;
                        data.meetingReminder = $sanitize(obj.meetingReminder);
                        patchData(endpoint, data).then(successCb, failureCb);
                    }

                    function updateThisUser() {
                        const endpoint = `https://hemmahealthquality.firebaseio.com/users/${adminCtrl.userToUpdate.key}`;
                        const data = {};
                        data.first = $sanitize(adminCtrl.userToUpdate.first);
                        data.last = $sanitize(adminCtrl.userToUpdate.last);
                        data.email = $sanitize(adminCtrl.userToUpdate.email);
                        data.role = adminCtrl.userToUpdate.role;
                        data.teams = adminCtrl.userToUpdate.teams;
                        patchData(endpoint, data).then(successCb, failureCb);
                    }

                    function deleteIt() {
                        adminDeleteUser(adminCtrl.userToDelete).then(successCb, failureCb);
                    }

                    function updateUser(row) {
                        adminCtrl.updateAlert = true;
                        adminCtrl.userToUpdate = angular.copy(row);
                        if(!adminCtrl.userToUpdate.teams){
                            adminCtrl.userToUpdate.teams = {};
                        }
                    }

                    function deleteUser(row) {
                        adminCtrl.deleteAlert = true;
                        adminCtrl.userToDelete = row;
                    }

                    function dontDelete() {
                        adminCtrl.deleteAlert = false;
                        adminCtrl.updateAlert = false;
                    }

                    function noDups(name) {
                        const filteredNames = adminCtrl.teamNames.filter((nameObj) => {
                            return name.toLowerCase() === nameObj.name.toLowerCase();
                        });

                        if (filteredNames.length) {
                            return false;
                        }
                        return true;
                    }

                    function addTeam() {
                        const endpoint = 'https://hemmahealthquality.firebaseio.com/admin/teamNames';
                        const data = {};
                        data.name = adminCtrl.model.newTeam;
                        data.label = adminCtrl.model.newTeam;
                        const noDuplicates = noDups(data.name);
                        if (!noDuplicates) {
                            adminCtrl.model.newTeam = '';
                            adminCtrl.alert = 'duplicateTeamName';
                            return;
                        }
                        postData(endpoint, data).then(init, failureCb);
                    }

                    function deleteTeam(data) {
                        const teamUrl = 'https://hemmahealthquality.firebaseio.com/admin/teamNames/';
                        const meetingUrl = 'https://hemmahealthquality.firebaseio.com/admin/dates/qualityMeeting/';

                        const teamPromise = deleteData(teamUrl, data.key);
                        const meetingPromise = deleteData(meetingUrl, data.name);
                        $q.all([teamPromise, meetingPromise]).then(init, failureCb);
                    }

                    adminCtrl.resetPassword = (user) => {
                        adminResetPassword(user).then(() => {
                            adminCtrl.alert = 'successfulPasswordReset';
                        }, () => {
                            adminCtrl.alert = 'error';
                        });
                    };

                    adminCtrl.updateThisUser = updateThisUser;
                    adminCtrl.dontDelete = dontDelete;
                    adminCtrl.updateUser = updateUser;
                    adminCtrl.deleteUser = deleteUser;
                    adminCtrl.deleteIt = deleteIt;
                    adminCtrl.updateMeetingOccurence = updateMeetingOccurence;
                    adminCtrl.addEmail = addEmail;
                    adminCtrl.addTeam = addTeam;
                    adminCtrl.deleteTeam = deleteTeam;

                    $rootScope.$on('updateAdminPageWithData', updateView);
                    init();
                }]
            }
        }
    ])
    .factory('getAdminData', ['$rootScope', 'getData', '$q', function ($rootScope, getData, $q) {
        return function () {
            function success(response) {
                $rootScope.$broadcast('updateAdminPageWithData', response);
            }

            function fail(err) {
                console.log('error', err);
            }

            const endpointAdmin = 'https://hemmahealthquality.firebaseio.com/admin';
            const endpointUsers = 'https://hemmahealthquality.firebaseio.com/users';
            const endpointRoles = 'https://hemmahealthquality.firebaseio.com/roles';

            const getAdmin = getData(endpointAdmin);
            const getUsers = getData(endpointUsers);
            const getRoles = getData(endpointRoles);

            $q.all([getAdmin, getUsers, getRoles]).then(success, fail);
        }
    }])
    .factory('adminDeleteUser', ['$q', '$http', function ($q, $http) {
        return function (data) {
            const deferred = $q.defer();
            firebase.auth().currentUser.getToken(true).then(function (idToken) {
                const endpoint = '/api/deleteUser';
                const config = {
                    method: 'post',
                    data: {
                        email: data.email,
                        token: idToken
                    },
                    url: endpoint
                };

                function successCb() {
                    deferred.resolve();
                }

                function failCb() {
                    deferred.reject();
                }

                function deleteFromRest() {
                    const url = `https://hemmahealthquality.firebaseio.com/users/${data.key}.json?auth=${idToken}`;
                    $http.delete(url).then(successCb, failCb);
                }

                $http(config).then(deleteFromRest, failCb);
            }).catch((error) => {
                deferred.reject(error);
            });

            return deferred.promise;
        }
    }])
    .factory('adminCreateUser', ['$q', '$http', function ($q, $http) {
        return function (data) {
            const deferred = $q.defer();

            if (data.email.length < 4) {
                deferred.reject('Please enter an email address.');
            }

            if (data.password.length < 4) {
                deferred.reject('Please enter a password.');
            }

            firebase.auth().currentUser.getToken(true).then(function (idToken) {
                const endpoint = '/api/createUser';
                const config = {
                    method: 'post',
                    data: {
                        email: data.email,
                        password: data.password,
                        token: idToken
                    },
                    url: endpoint
                };

                function successCb(res) {
                    if (res.data === 'auth/email-already-exists') {
                        deferred.reject('auth/email-already-exists')
                    } else {
                        deferred.resolve(data);
                    }
                }

                function failCb(res) {
                    deferred.reject();
                }

                $http(config).then(successCb, failCb);
            }).catch((error) => {
                // Handle Errors here.
                const errorCode = error.code;
                const errorMessage = error.message;
                // [START_EXCLUDE]
                if (errorCode == 'auth/weak-password') {
                    deferred.reject('The password is too weak.');
                } else {
                    deferred.reject(errorMessage);
                }
            });

            return deferred.promise;
        }
    }])
    .factory('adminResetPassword', ['$q', function ($q) {
        return function (data) {
            const deferred = $q.defer();
            const email = data.email;

            // [START sendpasswordemail]
            firebase.auth().sendPasswordResetEmail(email).then(function () {
                deferred.resolve('Password Reset Email Sent!');
            }).catch(function (error) {
                // Handle Errors here.
                const errorCode = error.code;
                const errorMessage = error.message;
                // [START_EXCLUDE]
                if (errorCode == 'auth/invalid-email') {
                    deferred.reject(errorMessage);
                } else if (errorCode == 'auth/user-not-found') {
                    deferred.reject(errorMessage);
                }
            });

            return deferred.promise;
        }
    }])
    .name;
