export default angular.module('pdsa.historicalPicker', [])
    .directive('historicPicker', ['pdasAgenda.getHistoricDates', function (getHistoricDates) {
        return {
            bindToController: true,
            controllerAs: 'histCtrl',
            controller: ['$scope', function ($scope) {
                const histCtrl = this;

                histCtrl.model = {};

                function loadHistoricDropDownCb(data) {
                    histCtrl.model.histListSelected = '--no selection--';
                    histCtrl.historicList = data.data;
                }

                function failCb() {
                    console.log('err');
                }

                function histChangeFn() {
                    $scope.$broadcast('historicDateChage', histCtrl.model.histListSelected);
                }

                function init() {
                    getHistoricDates().then(loadHistoricDropDownCb, failCb);
                }

                histCtrl.historicDateChange = histChangeFn;

                init();
            }],
            template: `<select class="form-control"
                ng-model="histCtrl.model.histListSelected"
                ng-change="histCtrl.historicDateChange()">
                <option>--no selection--</option>
                <option ng-repeat="date in histCtrl.historicList">{{date}}</option>
</select>`
        }
    }])
    .factory('pdasAgenda.getHistoricDates', ['$q', '$http', function ($q, $http) {
        return function () {
            const deferred = $q.defer();
            const endpoint = 'api/historical/list';

            $http.get(endpoint).then(deferred.resolve, deferred.reject);

            return deferred.promise;
        }
    }]).name;