require('./login.css');

import template from './login.html';
import signoutTemplate from './signout.html';

export default angular.module('directives.login', [])
    .directive('signout', ['$location', ($location) => {
        return {
            template: signoutTemplate,
            bindToController: true,
            controllerAs: 'signOutCtrl',
            replace: true,
            controller: [function () {
                const signOutCtrl = this;
                signOutCtrl.signOut = () => {
                    firebase.auth().signOut();
                    $location.path('/login');
                }
            }]
        }
    }])
    .directive('login', [
        'login.signIn',
        '$location',
        '$window',
        'firebaseAuthStateChange',
        function (signin, $location, $window, firebaseAuthStateChange) {
            return {
                template,
                bindToController: true,
                controllerAs: 'login',
                controller: ['$scope', function ($scope) {
                    const login = this;
                    login.signIn = signin;

                    firebaseAuthStateChange.subscribe(value => {
                        if (value) {
                            $window.location.href = '/home';
                        }
                    });

                    $scope.$on('$destroy', () => {
                        firebaseAuthStateChange.unsubscribe();
                    })
                }]
            }
        }
    ])
    .factory('login.signIn', [function () {
        return function () {
            if (firebase.auth().currentUser) {
                // [START signout]
                firebase.auth().signOut();
                // [END signout]
            } else {
                const email = document.getElementById('email').value;
                const password = document.getElementById('password').value;
                if (email.length < 4) {
                    alert('Please enter an email address.');
                    return;
                }
                if (password.length < 4) {
                    alert('Please enter a password.');
                    return;
                }
                // Sign in with email and pass.
                // [START authwithemail]
                firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
                    console.log(error);
                    // Handle Errors here.
                    const errorCode = error.code;
                    const errorMessage = error.message;
                    // [START_EXCLUDE]
                    const errText = 'Email and/or Password were wrong.\n \nIf you continue to have issues logging in please contact your account administrator.';
                    if (errorCode === 'auth/wrong-password') {
                        alert(errText);
                    } else {
                        alert(errText);
                    }
                    // console.log(error);
                    // document.getElementById('quickstart-sign-in').disabled = false;
                    // [END_EXCLUDE]
                });
                // [END authwithemail]
            }
            // document.getElementById('quickstart-sign-in').disabled = true;
        }
    }])
    .name;