const monthLookup = [
    'Janurary',
    'Feburary',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
];

const weekdayLookup =[
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
];

function convertMeetingObjToDateObj(meetingObj, _theDate){
    const theDate = (_theDate) ? new Date(_theDate) : new Date();
    const weekdayIndex = weekdayLookup.indexOf(meetingObj.dayOfWeek);
    const weekNumber = parseInt(meetingObj.weekOfMonth, 10) - 1;

    let weekdayArr = getWeekdaysInMonth(theDate, weekdayIndex);

    if (weekdayArr[weekNumber] >= theDate) {
        return weekdayArr[weekNumber];
    } else {
        weekdayArr = getWeekdaysInMonth(theDate.setMonth(theDate.getMonth() + 1), weekdayIndex);
        return weekdayArr[weekNumber];
    }
}

function convertMeetingObjToString(meetingObj) {
    const theDate = new Date();
    const weekdayIndex = weekdayLookup.indexOf(meetingObj.dayOfWeek);
    const weekNumber = parseInt(meetingObj.weekOfMonth, 10) - 1;

    let weekdayArr = getWeekdaysInMonth(new Date(), weekdayIndex);

    function dateObjToString(obj) {
        let str = '';
        str += `${weekdayLookup[obj.getDay()]} `;
        str += `${monthLookup[obj.getMonth()]} `;
        str += `${obj.getDate()}, `;
        str += `${obj.getFullYear()}`;
        return str;
    }

    if (weekdayArr[weekNumber] >= theDate) {
        return dateObjToString(weekdayArr[weekNumber]);
    } else {
        weekdayArr = getWeekdaysInMonth(theDate.setMonth(theDate.getMonth() + 1), weekdayIndex);
        return dateObjToString(weekdayArr[weekNumber]);
    }
}

function getWeekdaysInMonth(date, dayIndex) {
    var d = new Date(date),
        month = d.getMonth(),
        theDays = [];

    d.setDate(1);

    // Get the first Monday in the month
    while (d.getDay() !== dayIndex) {
        d.setDate(d.getDate() + 1);
    }

    // Get all the other Mondays in the month
    while (d.getMonth() === month) {
        theDays.push(new Date(d.getTime()));
        d.setDate(d.getDate() + 7);
    }

    return theDays;
}

module.exports = {
    convertMeetingObjToDateObj,
    convertMeetingObjToString,
    getWeekdaysInMonth,
    weekdayLookup,
    monthLookup
};
