require('./home.scss');
import homeTemplate from './home.html';

export default angular.module('homepage', [])
    .directive('home', ['$q', '$location', '$http', 'quarterEndMonthLookup', 'userRoleService', 'common.iterateObj',
        function ($q, $location, $http, quarterEndMonthLookup, userRoleService, iterateObj) {
            return {
                template: homeTemplate,
                bindToController: true,
                controllerAs: 'homeCtrl',
                controller: [function () {
                    const homeCtrl = this;
                    const currentUserObserve = userRoleService.currentUserObservable();
                    const taskGroupingTitles = {
                        pastDue: 'Past Due',
                        sevenDays: 'Due in Seven (7) Days',
                        thirtyDays: 'Due in Thirty (30) Days',
                        thisQuarter: 'Due in This Quarter',
                        everythingElse: 'All Other Tasks'
                    };
                    const taskGrouping = {
                        pastDue: [],
                        sevenDays: [],
                        thirtyDays: [],
                        thisQuarter: [],
                        everythingElse: []
                    };

                    function prepareTaskRows(taskArr) {
                        const today = new Date();
                        const sevenDays = Date.parse(new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7));
                        const thirtyDays = Date.parse(new Date(today.getFullYear(), today.getMonth(), today.getDate() + 30));
                        const thisQuarter = Date.parse(new Date(today.getFullYear(), quarterEndMonthLookup[today.getMonth()], today.getDate()));

                        taskArr.forEach((row) => {
                            row.jsDate = new Date(row.date);
                            return row;
                        });

                        taskArr.sort((a, b) => {
                            return b.jsDate - a.jsDate;
                        });

                        taskArr.forEach((row) => {
                            if (row.jsDate < today) {
                                taskGrouping.pastDue.push(row);
                            } else if (row.jsDate <= sevenDays) {
                                taskGrouping.sevenDays.push(row);
                            } else if (row.jsDate > sevenDays && row.jsDate <= thirtyDays) {
                                taskGrouping.thirtyDays.push(row);
                            } else if (row.jsDate > thirtyDays && row.jsDate <= thisQuarter) {
                                taskGrouping.thisQuarter.push(row);
                            } else {
                                taskGrouping.everythingElse.push(row);
                            }
                        });

                        homeCtrl.taskGrouping = taskGrouping;
                        homeCtrl.taskGroupingTitles = taskGroupingTitles;
                    }

                    function openThisTask(taskObj) {
                        $location.url(`/quality/pdsa/${taskObj.team}/update?key=${taskObj.parent}`);
                    }

                    function storePdsaTasks(data) {
                        let response = [];
                        for( let[taskName, tasks] of iterateObj(data)){
                            tasks.data.forEach((task) => {task.team = taskName});
                            response = response.concat(tasks.data);
                        }

                        prepareTaskRows(response);
                    }

                    function failureCb() {
                        console.log('error');
                    }

                    function init(user) {
                        const promiseArr = {};

                        for( let[key, value] of iterateObj(user.teams)){
                            const url = `/api/forms/pdsa/${key}/tasks`;
                            promiseArr[key] = $http.get(url);
                        }

                       $q.all(promiseArr).then(storePdsaTasks, failureCb);
                    }

                    homeCtrl.viewThisTask = openThisTask;

                    currentUserObserve.subscribe(value => {
                        init(value);
                    });
                }]
            }
        }])
    .constant('quarterEndMonthLookup', {
        qrt: {
            0: 2,
            1: 2,
            2: 2,
            3: 5,
            4: 5,
            5: 5,
            6: 8,
            7: 8,
            8: 8,
            9: 11,
            10: 11,
            11: 11
        }
    })
    .name;