const histFns = require('./historicFns');

function createHistoricalSnapshot() {
    return new Promise(function (resolve, reject) {
        function postSnapshot(_snapshot) {
            const postPromise = histFns.postNewHistoricObj(_snapshot);
            postPromise.then(resolve, reject);
        }

        const cleanSnapshot = function (_snap) {
            const cleanSnapshotObj = histFns.cleanSnapshot(_snap);
            postSnapshot(cleanSnapshotObj);
        };

        const dbSnapshot = histFns.getCopyOfWholeDb();
        dbSnapshot.once('value', cleanSnapshot, reject);
    })
}

module.exports = {
    createHistoricalSnapshot,
    "getHistoricalDates": histFns.getHistoricalDates
};
