// link to agenda
// setup site to remember what path was used before login
// list task by specific user / email
// get meeting date

const admin = require("firebase-admin");
const getPdsaPlanRowTasksnpm  = require('./getPdsaTasks.js');
const getAdminInfo = require('./getAdminInfo.js');
const getUsers = require('./getUsers.js');
const sendEmail = require('./sendEmail.js');

function ensureTaskIsDueByNextMeeting(dueDate, taskObj) {
    const response = JSON.parse(JSON.stringify(taskObj));
    const users = Object.keys(taskObj);

    function checkTasks(aim, user) {
        const thisTaskArr = response[user][aim];
        const theKeepers = [];
        thisTaskArr.forEach((task, i) => {
            const thisTaskDate = new Date(task.date);
            if (thisTaskDate.getTime() <= dueDate) {
                theKeepers.push(task);
            }
        });

        if (theKeepers.length) {
            response[user][aim] = theKeepers;
        } else {
            delete response[user][aim];
        }
    }

    function goThurTasks(user) {
        const usersAim = Object.keys(taskObj[user]);
        usersAim.forEach((aim) => {
            checkTasks(aim, user);
        })
    }

    users.forEach(goThurTasks);
    return response;
}

function getBody(theirPdsas){
    const pdsaTitles = Object.keys(theirPdsas);
    let response = '<dl>';

    function buildPdsaSectionDetails(task){
        response = response + '<dd>' + task.task + '</dd>';
    }

    function buildPdsaSectionTitle (title) {
        response = response + '<dt>' + title + '</dt>';
        theirPdsas[title].forEach(buildPdsaSectionDetails);
    }

    pdsaTitles.forEach(buildPdsaSectionTitle);
    return response + '</dl>';
}

function getMessageHeading(person) {
    return '<h3>Hello, ' + person + '</h3>' + "<p>Please see PDSA's and related tasks assigned to you for the up coming Quality Meeting.</p>";
}

function getHtml(person, theirPdsas) {
    const pdsas = Object.keys(theirPdsas);
    if (!pdsas.length) {
        return false;
    }

    let response = '';
    response = response + getMessageHeading(person);
    response = response + getBody(theirPdsas);

    return response;
}

function aggregateResponses(response) {
    const date = response[0];
    const peopleWithTasks = response[1];
    const users = response[2];
    const stuffDueThisMonth = ensureTaskIsDueByNextMeeting(date, peopleWithTasks);
    const people = Object.keys(stuffDueThisMonth);
    const promises = [];

    people.forEach((person) => {
        const html = getHtml(person, stuffDueThisMonth[person]);
        console.log(person);
        const subject = 'Quality Meeting Alert - What is due?';
        const to = users[person].email;

        if (!html) {
            return;
        }

        // promises.push(sendEmail.sendEmail([to], subject, html));
    });

    // Promise.all(promises).then(endProcess, endProcess);
}

function failed(err) {
    console.log('failure', err);
    process.exit(0);
}

function triggerEmails() {
    const getTasksPromise = getPdsaPlanRowTasks.getTasksByUser();
    const getAdminPromise = getAdminInfo.getDateTimeOfMonthlyQiMeeting();
    const getAllUsers = getUsers.getAllUsers();


    endProcess()
    // Promise.all([getAdminPromise, getTasksPromise, getAllUsers]).then(aggregateResponses, failed);
}

function endProcess() {
    process.exit(0);
}

module.exports = {
    triggerEmails
};
