const admin = require("firebase-admin");

function createUser(email, password) {
    return new Promise((resolve, reject) => {
        admin.auth()
            .createUser({
                email,
                emailVerified: false,
                password
            })
            .then(function () {
                resolve();
            })
            .catch(function (error) {
                if (error.code === 'auth/email-already-exists') {
                    reject('auth/email-already-exists');
                } else {
                    reject(error);
                }
            });
    });
}

// function updateUser() {
//     admin.auth()
//         .updateUser(uid, {
//             email: "modifiedUser@example.com",
//             emailVerified: true,
//             password: "newPassword",
//             displayName: "Jane Doe",
//             photoURL: "http://www.example.com/12345678/photo.png",
//             disabled: true
//         })
//         .then(function (userRecord) {
//             // See the UserRecord reference doc for the contents of userRecord.
//             console.log("Successfully updated user", userRecord.toJSON());
//         })
//         .catch(function (error) {
//             console.log("Error updating user:", error);
//         });
// }

function deleteUser(email) {
    return new Promise((resolve, reject) => {

        function deleteUserByUid(uid) {
            admin.auth()
                .deleteUser(uid)
                .then(function () {
                    resolve();
                })
                .catch(function (error) {
                    reject(error);
                });
        }

        function getUserData(email) {
            admin.auth()
                .getUserByEmail(email)
                .then(function (userRecord) {
                    if (userRecord.uid) {
                        deleteUserByUid(userRecord.uid);
                    } else {
                        reject('cannot find uid');
                    }
                })
                .catch(function (error) {
                    reject(error);
                });
        }

        getUserData(email);
    })
}

module.exports = {
    createUser,
    deleteUser
};
