const admin = require("firebase-admin");

function doGet() {
    const db = admin.database();
    const users = db.ref('/users');
    return users.once('value');
}

function getAllUsers(){
    return new Promise((resolve, reject) => {
        function success(_snapshot){
            const snapshot = _snapshot.val();
            const response = {};
            const pdsas = Object.keys(snapshot);
            const createResponse = (key) => {
                const thePdsa = snapshot[key];
                const newKey = thePdsa.first + ' ' + thePdsa.last;
                response[newKey] = {
                    email: thePdsa.email,
                    roleValue: thePdsa.roleValue
                }
            };

            pdsas.forEach(createResponse);
            console.log(response);
            resolve(response);
        }

        function fail(){
            reject('couldntGetUsers');
        }
        doGet().then(success, fail);
    });
}

module.exports = {
    getAllUsers
};
