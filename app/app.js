require('./app.scss');
import ngRoute from 'angular-route';
import ngSanitize from 'angular-sanitize';
import login from './pages/login';
import reports from './pages/reports/reports';
import header from './components/header/header';
import breadcrumbs from './components/breadcrumbs/breadcrumbs';
import admin from './pages/admin/admin';
import loading from './components/loading/loading';
import flashAlert from './components/flashAlert/flashAlert';
import home from './pages/home/home';
import sidebar from './components/sidebar/sidebar';
import howTo from './pages/howTo/howTo';
import adminTeamMeeting from './pages/admin/admin-team-meeting/admin-team-meeting';
import commonUiFns from './commonUiFn';

angular
    .module("app", [
        ngRoute,
        ngSanitize,
        'firebase',
        'rx',
        loading,
        login,
        reports,
        header,
        breadcrumbs,
        admin,
        flashAlert,
        home,
        sidebar,
        howTo,
        adminTeamMeeting,
        commonUiFns
    ])
    .run(['$rootScope', '$location', function ($rootScope, $location) {
        $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
            if (rejection === 'NOT_AUTHORIZED') {
                $location.path('/login');
            } else if (rejection === 'AUTH_REQUIRED') {
                $location.path('/login');
            }
        });

        const config = {
            apiKey: "AIzaSyBCb-WcqBL68LpEoW3SCxF0p-7XSRITCYc",
            authDomain: "hemmahealthquality.firebaseapp.com",
            databaseURL: "https://hemmahealthquality.firebaseio.com",
            projectId: "hemmahealthquality",
            storageBucket: "hemmahealthquality.appspot.com",
            messagingSenderId: "962984558826"
        };

        firebase.initializeApp(config);
    }])
    .controller('MainCtrl', ['$scope', '$route', '$timeout', 'firebase', 'firebaseAuthStateChange',
        function ($scope, $route, $timeout, firebase, firebaseAuthStateChange) {
            let listener;
            $scope.showSidebar = false;

            firebaseAuthStateChange.subscribe(value => {
                console.log('subscribe');
                if (value) {
                    listener = $scope.$on('$routeChangeSuccess', () => {
                        $scope.showSidebar = value;
                        listener();
                    });
                } else {
                    $timeout(() => {
                        $scope.showSidebar = value;
                    }, 0);
                }
            });

            $scope.$on('$destroy', () => {
                firebaseAuthStateChange.unsubscribe();
            })
        }]
    )
    .service('firebaseAuthStateChange', ['firebase', 'rx', function (firebase, rx) {
        const stateObservable = rx.Observable.create(observer => {
            let signedInStatus = false;

            firebase.auth().onAuthStateChanged(function (user) {
                if (user) {
                    // User is signed in.
                    signedInStatus = true;
                    observer.next(signedInStatus);
                } else {
                    // No user is signed in.
                    signedInStatus = false;
                    observer.next(signedInStatus);
                }
            });
        });
        return stateObservable;
    }])
    .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        $routeProvider
            .when('/login', {
                template: '<div login></div>',
                class: 'login-background'
            })
            .when('/home', {
                template: '<div home></div>',
                class: 'home-page',
                resolve: {
                    // controller will not be loaded until $requireSignIn resolves
                    // Auth refers to our $firebaseAuth wrapper in the factory below
                    "currentAuth": ["Auth", function (Auth) {
                        // $requireSignIn returns a promise so the resolve waits for it to complete
                        // If the promise is rejected, it will throw a $routeChangeError (see above)
                        return Auth.$requireSignIn();
                    }]
                }
            })
            .when('/admin', {
                template: '<div admin-home></div>',
                class: 'admin-page',
                resolve: {
                    // controller will not be loaded until $requireSignIn resolves
                    // Auth refers to our $firebaseAuth wrapper in the factory below
                    "currentAuth": ["Auth", function (Auth) {
                        // $requireSignIn returns a promise so the resolve waits for it to complete
                        // If the promise is rejected, it will throw a $routeChangeError (see above)
                        return Auth.$requireSignIn();
                    }]
                }
            })
            .when('/quality/pdsa/:team/:tab?', {
                template: '<div pdsa-home></div>',
                class:'pdsa-page',
                resolve: {
                    // controller will not be loaded until $requireSignIn resolves
                    // Auth refers to our $firebaseAuth wrapper in the factory below
                    "currentAuth": ["Auth", function (Auth) {
                        // $requireSignIn returns a promise so the resolve waits for it to complete
                        // If the promise is rejected, it will throw a $routeChangeError (see above)
                        return Auth.$requireSignIn();
                    }]
                }
            })
            .when('/quality', {
                template: '<div report-home></div>',
                class: 'reports-page',
                resolve: {
                    // controller will not be loaded until $requireSignIn resolves
                    // Auth refers to our $firebaseAuth wrapper in the factory below
                    "currentAuth": ["Auth", function (Auth) {
                        // $requireSignIn returns a promise so the resolve waits for it to complete
                        // If the promise is rejected, it will throw a $routeChangeError (see above)
                        return Auth.$requireSignIn();
                    }]
                }
            })
            .when('/how-to', {
                template: '<div how-to></div>',
                class: 'how-to-page',
                resolve: {
                    "currentAuth": ["Auth", function (Auth) {
                        return Auth.$requireSignIn();
                    }]
                }
            })
            .otherwise('/login');
        $locationProvider.html5Mode(true);
    }])
    .directive('classRoute', [
        '$location',
        '$rootScope',
        function ($location, $rootScope) {
            return function (scope, elem, attr) {
                let previous = '';
                $rootScope.$on('$routeChangeSuccess', function (event, currentRoute) {

                    const route = currentRoute.$$route;
                    if (route) {
                        const cls = route['class'];

                        if (previous) {
                            attr.$removeClass(previous);
                        }

                        if (cls) {
                            previous = cls;
                            attr.$addClass(cls);
                        }
                    }
                });
            };
        }
    ])
    .factory("Auth", ["$firebaseAuth", function ($firebaseAuth) {
        return $firebaseAuth();
    }])
    .factory('qualityCheckUserRole', ['$q', '$http', '$rootScope', function ($q, $http, $rootScope) {
        return function () {
            const deferred = $q.defer();

            function doGet(idToken) {
                const url = 'https://hemmahealthquality.firebaseio.com/users' +
                    '.json?print=pretty&auth=' + idToken;

                function successCb(result) {
                    deferred.resolve(result);
                }

                function failCb() {
                    deferred.reject('');
                }

                $http.get(url).then(successCb, failCb);
            }

            firebase.auth().currentUser.getToken(true).then(function (idToken) {
                doGet(idToken);
            }).catch(function () {
                userRoleService.setRoles('');
            });

            return deferred.promise;
        }
    }])
    .factory('pdsa.getAdminData', ['getData', function (getData) {
        return function () {
            const getDataPromise = getData('https://hemmahealthquality.firebaseio.com/admin');
            return getDataPromise;
        }
    }])
    .service('userRoleService', [
        '$q',
        'rx',
        'firebase',
        'qualityCheckUserRole',
        function ($q, rx, firebase, qualityCheckUserRole) {
            let users;
            let currentUser;
            const user = firebase.auth().currentUser;
            const userChangeMethods = {
                observer: null,
                setObserver: (observer) => {
                    this.observer = observer
                },
                triggerNext: (user) => {
                    if(this.observer){
                        this.observer.next(user);
                    }
                }
            };

            function findOutCurrentUser() {
                const keys = Object.keys(users);
                const _oldUser = currentUser;
                currentUser = null;
                keys.forEach(function (key) {
                    if (users[key].email === user.email) {
                        currentUser = users[key];
                    }
                });

                userChangeMethods.triggerNext(currentUser);

                return currentUser;
            }

            function setRoles(data) {
                if (data) {
                    users = data.data;
                } else {
                    users = '';
                }
            }

            this.currentUserObservable = () => {
                return rx.Observable.create(observer => {
                    userChangeMethods.setObserver(observer);
                });
            };

            this.getCurrentUserRoles = () => {
                const deferred = $q.defer();

                function returnUser(result) {
                    setRoles(result);
                    const current = findOutCurrentUser();
                    deferred.resolve(current)
                }

                function clearUser() {
                    setRoles();
                    findOutCurrentUser();
                    deferred.reject();
                }

                function init() {
                    if (!users) {
                        qualityCheckUserRole().then(returnUser, clearUser);
                    } else {
                        const current = findOutCurrentUser();
                        deferred.resolve(current);
                    }
                }

                init();
                return deferred.promise;
            };

            this.getRoles = () => {
                return users;
            };
        }])
    .factory('getData', ['$http', '$q', function ($http, $q) {
        return function (endpoint) {
            const deferred = $q.defer();

            function doGet(idToken) {
                const url = `${endpoint}.json?print=pretty&auth=${idToken}`;

                function successCb(result) {
                    deferred.resolve(result);
                }

                function failCb() {
                    deferred.reject('error');
                }

                $http.get(url).then(successCb, failCb);
            }

            firebase.auth().currentUser.getToken(true).then(function (idToken) {
                doGet(idToken);
            }).catch(function () {
                deferred.reject();
            });

            return deferred.promise;
        }
    }])
    .factory('patchData', ['$http', '$q', 'firebase', function ($http, $q, firebase) {
        return function (endpoint, data) {
            const deferred = $q.defer();

            function successCb(response) {
                deferred.resolve(response);
            }

            function failureCb(err) {
                deferred.reject(err)
            }

            firebase.auth().currentUser.getToken(true).then(function (idToken) {
                const url = `${endpoint}/.json?&auth=${idToken}`;
                $http.patch(url, data).then(successCb, failureCb);
            }).catch(function () {
                failureCb('not authenticated');
            });

            return deferred.promise;
        }
    }])
    .factory('deleteData', ['$http', '$q', 'firebase', function ($http, $q, firebase) {
        return function (endpoint, key) {
            const deferred = $q.defer();

            function successCb(response) {
                deferred.resolve(response);
            }

            function failureCb(err) {
                deferred.reject(err)
            }

            firebase.auth().currentUser.getToken(true).then(function (idToken) {
                const url = `${endpoint}/${key}.json?&auth=${idToken}`;
                $http.delete(url).then(successCb, failureCb);
            }).catch(function () {
                failureCb('not authenticated');
            });

            return deferred.promise;
        }
    }])
    .factory('postData', ['$http', '$q', 'firebase', function ($http, $q, firebase) {
        return function (endpoint, data) {
            const deferred = $q.defer();

            function successCb(response) {
                deferred.resolve(response);
            }

            function failureCb(err) {
                deferred.reject(err)
            }

            firebase.auth().currentUser.getToken(true).then(function (idToken) {
                const url = `${endpoint}/.json?&auth=${idToken}`;
                $http.post(url, data).then(successCb, failureCb);
            }).catch(function () {
                failureCb('not authenticated');
            });

            return deferred.promise;
        }
    }])
    .constant('weekdayLookup', [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
    ])
    .constant('monthLookup', [
        'Janurary',
        'Feburary',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ]);
