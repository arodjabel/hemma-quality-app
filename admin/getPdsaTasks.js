const admin = require("firebase-admin");

function getPdsa(team) {
    const db = admin.database();
    const planOnes = db.ref('/forms/pdsa/' + team);
    return planOnes.orderByChild('planOne');
}

function getTasksByUser() {
    return new Promise((resolve, reject) => {
        function groupByUser(_snapshot) {
            const snapshot = _snapshot.val();
            const pdsaKeys = Object.keys(snapshot);
            let allTasks = {};

            function getTaskArray(key) {
                let row;
                let tempArr;
                row = snapshot[key];
                if (row.deactive || !row.planOne) {
                    return;
                }
                row.pdsaKey = key;

                row.planOne.forEach((task) => {
                    if (task.taskComplete) {
                        return;
                    }
                    if (!allTasks[task.party.label]) {
                        allTasks[task.party.label] = {};
                    }

                    if (!allTasks[task.party.label][row.aim]) {
                        allTasks[task.party.label][row.aim] = [];
                    }

                    allTasks[task.party.label][row.aim].push(task);
                });

            }

            pdsaKeys.forEach(getTaskArray);
            resolve(allTasks);
        }

        function failureCb(errorObject) {
            reject(errorObject.code);
        }

        getPdsa().once("value", groupByUser, failureCb);
    })
}

function getTasksByChild(team) {
    return new Promise((resolve, reject) => {
        function makeTaskResponseArray(snapshot) {
            const data = snapshot.val();
            const pdsaKeys = Object.keys(data);
            let response = [];
            pdsaKeys.forEach((key) => {
                if (data[key].deactive) {
                    return;
                }
                if (data[key].planOne) {
                    data[key].planOne.forEach((task) => {
                        task.parent = key;
                    });
                    response = response.concat(data[key].planOne);
                }
            });

            resolve(response);
        }

        function failureCb(errorObject) {
            reject(errorObject.code);
        }

        getPdsa(team).once("value", makeTaskResponseArray, failureCb);
    })
}

module.exports = {
    getTasksByChild,
    getTasksByUser
};
