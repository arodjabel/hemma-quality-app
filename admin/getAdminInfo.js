const admin = require("firebase-admin");
const common = require('./commonFn.js');

function getAllAdmin() {
    const db = admin.database();
    const adminInfo = db.ref('/admin/dates/qualityMeeting');
    return adminInfo.once('value');
}

function isTheMeetingSupposedToHappenXTimeFromNow(meetingDate, monthObj) {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    meetingDate.setHours(0, 0, 0, 0);
    const reminderDate = new Date(meetingDate.getTime());
    reminderDate.setHours(0, 0, 0, 0);
    reminderDate.setDate(meetingDate.getDate() - parseInt(monthObj.meetingReminder));
    // console.log(reminderDate, today, meetingDate);
    // console.log(reminderDate.getTime() === today.getTime());
    return reminderDate.getTime() === today.getTime();
}

function getDateTimeOfMonthlyQiMeeting() {
    return new Promise((resolve, reject) => {
        function success(_snapshot) {
            const monthObj = _snapshot.val();
            if (monthObj.meetingReminder === 'doNotSend') {
                reject('doNotSend');
            }

            const date = common.convertMeetingObjToDateObj(monthObj);

            if (isTheMeetingSupposedToHappenXTimeFromNow(date, monthObj)) {
                resolve(date);
            } else {
                reject('notTimeForReminders');
            }
        }

        function fail(err) {
            reject(err)
        }

        getAllAdmin().then(success, fail);
    })
}

module.exports = {
    getAllAdmin,
    getDateTimeOfMonthlyQiMeeting
};